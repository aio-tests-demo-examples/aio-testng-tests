# How to use this repo #

The repo is to demo how TestNG tests can be mapped with AIO Test Keys.  It also has a sample listener which shows how AIO Tests APIs can be invoked in the TestNG hooks.

Check out the listener @ src/test/java/com/aio/auto/listeners/
Look at samples of how to map cases @ src/test/java/com/aio/auto/samples/


### Queries ###

Read more about [AIO Tests - TestNG Integration](https://aioreports.atlassian.net/wiki/spaces/ATDoc/pages/484049081/TestNG).

* help@aioreports.com