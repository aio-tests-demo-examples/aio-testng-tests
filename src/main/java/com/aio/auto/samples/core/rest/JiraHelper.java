package com.aio.auto.samples.core.rest;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

public class JiraHelper extends APIHelper{
    private static RequestSpecification defaultRequestSpec;
    private static JiraHelper jiraHelper;
    static {
        RestAssured.baseURI = "https://priyankaraghav.atlassian.net";
        RestAssured.basePath = "/rest/api/latest";
        RequestSpecBuilder builder   = new RequestSpecBuilder();
        builder.addHeader("Content-Type","application/json");
        builder.addHeader("Authorization", "Basic bmloYXJpa2EudmFyc2huZXlAZ21haWwuY29tOjdacjluZ0RFeXJZYUkzY3ZqOFkzNUQ1OA==");
        builder.log(LogDetail.METHOD).log(LogDetail.URI).log(LogDetail.BODY);
        defaultRequestSpec = builder.build();
    }

    private JiraHelper() { }

    @Override
     RequestSpecification getReqSpec() {
        return defaultRequestSpec;
    }

    public static JiraHelper get() {
        if(jiraHelper == null) {
            jiraHelper = new JiraHelper();
        }
        return jiraHelper;
    }

    public String getIssueId(String issueKey) {
        System.out.println("Fetching issue id for " + issueKey);
        Response response =  this.doGet(JIRA_URLS.GET_ISSUE, issueKey);
        return response.jsonPath().getString("id");
    }

    public  void closeIssue(String issueIdOrKey) {
        Map<String, Object> jiraKey = new HashMap<>();
        jiraKey.put("issueIdOrKey", issueIdOrKey);
        Response r = this.doPostWithAPIBody(JIRA_URLS.CLOSE_ISSUE, jiraKey);
        r.prettyPrint();
    }

    public  void moveToInProgress(String issueIdOrKey) {
        Map<String, Object> jiraKey = new HashMap<>();
        jiraKey.put("issueIdOrKey", issueIdOrKey);
        Response r = this.doPostWithAPIBody(JIRA_URLS.TRANSITION_TO_INPROGRESS, jiraKey);
        r.prettyPrint();
    }
}
