package com.aio.auto.samples.core.rest;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.Map;

import static io.restassured.RestAssured.given;

public abstract class APIHelper {
     abstract  RequestSpecification getReqSpec() ;

    public  Response doGet(RestAPIDetail restAPI) {
        Response response = given(this.getReqSpec()).when().get(restAPI.getPath()).andReturn();
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }

    Response doGet(RestAPIDetail restAPI, String...pathParams) {
        Response response = given(this.getReqSpec()).when().get(restAPI.getPath(),pathParams).andReturn();
        System.out.println(response.statusCode());
//        response.body().prettyPrint();
        return response;
    }

    public  Response doGet(RestAPIDetail restAPI, Map<String, Object> params) {
        Response response = given(this.getReqSpec()).when().get(restAPI.getPath(),params).andReturn();
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }

    Response doPostWithAPIBody(RestAPIDetail restAPI,  Map<String, Object> params) {
        Response response = given(this.getReqSpec()).
                contentType("application/json").
                body(restAPI.getBody()).when().post(restAPI.getPath(),params).andReturn();
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }

    public  Response doPost(RestAPIDetail restAPI, Object body, String...pathParams) {
        Response response = given(this.getReqSpec()).
                contentType("application/json").
                body(body).when().post(restAPI.getPath(),pathParams).andReturn();
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }

    public  Response doPost(RestAPIDetail restAPI, Map<String, Object> params, String...pathParams) {
        Response response = given(this.getReqSpec()).contentType(ContentType.JSON).body(params).when().post(restAPI.getPath(),pathParams).andReturn();
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }

    public  Response doPostWithReplaceParams(RestAPIDetail restAPI, Map<String, Object> params) {
        Response response = given(this.getReqSpec()).when().post(restAPI.getPath(),params).andReturn();

        System.out.println(response.getStatusLine());
        System.out.println(response.statusCode());
        response.body().prettyPrint();
        return response;
    }
}
