package com.aio.auto.samples.core.rest;

class JIRA_URLS {

    static final RestAPIDetail GET_ISSUE = new RestAPIDetail("issue/{issueKey}");
    static final RestAPIDetail TRANSITION_TO_INPROGRESS = new RestAPIDetail("issue/{issueIdOrKey}/transitions","{\"transition\":\"21\"}");
    static final RestAPIDetail CLOSE_ISSUE = new RestAPIDetail("issue/{issueIdOrKey}/transitions","{\"transition\":\"31\"}");

}
