package com.aiotests.automation;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.OutputType;

import org.openqa.selenium.TakesScreenshot;

import java.io.File;

public class DriverUtils {
    public static File takeScreenshot() {
        try {
            byte[] screenShotData = ((TakesScreenshot) DriverManager.get()).getScreenshotAs(OutputType.BYTES);
            File screenshotFile =
                    new File("./"  + RandomStringUtils.randomAlphanumeric(10) + ".png");

            FileUtils.writeByteArrayToFile(screenshotFile, screenShotData);
            return screenshotFile;

        } catch (Throwable e) {
            System.out.println("Screenshot could not be taken or saved");
        }
        return null;
    }
}
