package com.aiotests.automation;

import com.aio.auto.samples.core.rest.AioAPIHelper;
import com.beust.jcommander.internal.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.asserts.Assertion;
import org.testng.asserts.IAssert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SoftAsserts extends Assertion {
    private final Map<AssertionError, IAssert<?>> allErrors = Maps.newLinkedHashMap();
    private static ThreadLocal<SoftAsserts> softAssert = new ThreadLocal<SoftAsserts>();

    public static SoftAsserts getSoftAssert() {
        if (softAssert.get() == null) {
            // this is need when running tests from IDE
            System.out.println("Initializing soft asserts for thread");
            setAssertion();
        }
        return softAssert.get();
    }

    public static void setAssertion() {
        softAssert.remove();
        softAssert.set(new SoftAsserts());
    }

    @Override
    protected void doAssert(IAssert<?> assertCommand) {
        try {
            executeAssert(assertCommand);
        } finally {
            onAfterAssert(assertCommand);
        }
    }

    @Override
    public void executeAssert(IAssert<?> a) {
        try {
            a.doAssert();
            onAssertSuccess(a);
        } catch (AssertionError ex) {
            onAssertFailure(a, ex);
            allErrors.put(ex, a);
        }
    }


    @Override
    public void onAssertSuccess(IAssert<?> assertCommand) {
        if (assertCommand.getMessage() != null && !assertCommand.getMessage().trim().isEmpty()) {
            String text = "Assertion passed : " + assertCommand.getMessage();
            //Instead of sout it can be your logger
            System.out.println(text);
            addToTestComments(text);
        }

    }

    @Override
    public void onAssertFailure(IAssert<?> assertCommand, AssertionError ex) {
        String trace = "Failed : " + assertCommand.getMessage();

        trace += ExceptionUtils.getStackTrace(ex);
        trace = StringUtils.substringBetween(trace, "\n",
                "at sun.reflect.NativeMethodAccessorImpl.invoke0").replace("\t", "\t\t");
        System.out.println(trace);
        addToTestComments(trace);
        if(DriverManager.get() != null) {
            File  screenshot = DriverUtils.takeScreenshot();
            List<String> aioCaseKeys = AIOUtils.getCaseKeyFromGroupsIfAny(Reporter.getCurrentTestResult().getMethod().getGroups());
            if(aioCaseKeys != null) {
                aioCaseKeys.forEach(aioCaseKey -> AioAPIHelper.uploadAttachment(AIOUtils.PROJECT_KEY, AIOUtils.CYCLE_KEY, aioCaseKey, screenshot));
            }
        }

    }

    private void addToTestComments(String text) {
        Object currentCommentsList = Reporter.getCurrentTestResult().getAttribute("comments");
        if(currentCommentsList == null) {
            Reporter.getCurrentTestResult().setAttribute("comments", new ArrayList<>());
        }
        ((List)Reporter.getCurrentTestResult().getAttribute("comments")).add(text);
    }
    private String showAssertInfo(IAssert<?> assertCommand, AssertionError ex, boolean failedTest) {
        ITestResult testResult = Reporter.getCurrentTestResult();

        // Checks whether the soft assert was called in a TestNG test run or else within a Java application.
        String methodName = "main";
        if (testResult != null) {
            methodName = testResult.getMethod().getMethodName();
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Soft Assert ");
        if (assertCommand.getMessage() != null && !assertCommand.getMessage().trim().isEmpty()) {
            sb.append("[").append(assertCommand.getMessage()).append("] ");
        }
        if (failedTest) {
            sb.append("failed");
        } else {
            sb.append("passed");
        }
        if (failedTest) {
            sb.append(ExceptionUtils.getStackTrace(ex));
        }
        Reporter.log(sb.toString(), true);
        return sb.toString();
    }
}
