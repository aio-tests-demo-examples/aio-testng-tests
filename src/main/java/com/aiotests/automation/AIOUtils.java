package com.aiotests.automation;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AIOUtils {

    /**
     * Hardcoded values - these values can come from an env variable or a property file
     */
    public final static String PROJECT_KEY = "NVTES";
    public final static String CYCLE_KEY = "NVTES-CY-126";

    /**
     * If groups are found then it reports to those cases
     * @param groups
     * @return
     */
    public static List<String> getCaseKeyFromGroupsIfAny(String[] groups) {
        if (groups != null) {
            return Arrays.stream(groups).filter(group -> group.trim().startsWith(PROJECT_KEY + "-TC-")).map(g -> g.trim())
                    .collect(Collectors.toList());
        }
        return null;

    }
}
