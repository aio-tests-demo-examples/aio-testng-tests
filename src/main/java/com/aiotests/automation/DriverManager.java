package com.aiotests.automation;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DriverManager {
    private static ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();

    public static void init() {
        if (driver.get() == null) {
            // this is need when running tests from IDE
            driver.set(new ChromeDriver());
        }
    }
    public static WebDriver get() {
        return driver.get();
    }

    public void quit() {
        driver.get().quit();
    }
}