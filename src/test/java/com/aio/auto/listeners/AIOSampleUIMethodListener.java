package com.aio.auto.listeners;

import com.aio.auto.samples.core.rest.AioAPIHelper;
import com.aiotests.automation.AIOUtils;
import com.aiotests.automation.DriverManager;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.util.List;

public class AIOSampleUIMethodListener implements IInvokedMethodListener {

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        DriverManager.init();
        List<String> caseKeys = AIOUtils.getCaseKeyFromGroupsIfAny(method.getTestMethod().getGroups());
        if(caseKeys != null) {
            caseKeys.forEach(cK -> AioAPIHelper.addCase(AIOUtils.PROJECT_KEY, AIOUtils.CYCLE_KEY, cK));
        }
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        List<String> caseKeys = AIOUtils.getCaseKeyFromGroupsIfAny(method.getTestMethod().getGroups());
        if(caseKeys != null) {
            String status = getStatus(testResult.getStatus());
            Long duration = testResult.getEndMillis() - testResult.getStartMillis();
            AioAPIHelper.markCaseStatus(AIOUtils.PROJECT_KEY, AIOUtils.CYCLE_KEY, caseKeys,
                    (List)testResult.getAttribute("comments"), status, duration);
        }
        DriverManager.get().quit();
    }

    private String getStatus(Integer i) {
        switch (i) {
            case 1: return "Passed";
            case 2: return "Failed";
            case 3: return "Not Run";
        }
        return "Not Run";
    }
}
