package com.aio.auto.listeners;

import com.aio.auto.samples.core.rest.AioAPIHelper;
import junit.framework.TestResult;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestContext;
import org.testng.ITestResult;

import java.util.Collections;

public class AIOMethodListener implements IInvokedMethodListener {

    private String projectKey = "NVTES";
    private String cycleKey = "NVTES-CY-126";

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if(method.getTestMethod().getGroups().length > 0) {
            for(String t: method.getTestMethod().getGroups()) {
                if(t.startsWith( this.projectKey + "-TC-")){
                    System.out.println(t);
                    String status = getStatus(testResult.getStatus());
                    Long duration = testResult.getEndMillis() - testResult.getStartMillis();
                    AioAPIHelper.markCaseStatus(this.projectKey, this.cycleKey, Collections.singletonList(t),
                            Collections.singletonList("some comment")
                            , status, duration);
                }
            }
        }
    }

    private String getStatus(Integer i) {
        switch (i) {
            case 1: return "Passed";
            case 2: return "Failed";
            case 3: return "Not Run";
        }
        return "Not Run";
    }
}
