package com.aio.auto.samples;
import org.testng.Assert;
import org.testng.annotations.CustomAttribute;
import org.testng.annotations.Test;

public class TestsWithGroups {


    @Test( groups = {"NVTES-TC-164"}, description = "Create a HUK Request in Salesforce/CMS.")
    public void failingTest2() {
        Assert.assertEquals(0,1, "done");
    }

    @Test(  groups = {"NVTES-TC-163"}, description = "HR Ops: Create a FGY AOI Request in Salesforce.",
            attributes = {@CustomAttribute(name="aio_key", values = "tcname")})
    public void failingTest3() {
        Assert.assertEquals(0,0, "done");
    }

    @Test( groups= {"P0"}, description = "Passing case")
    public void passingCase() {
        Assert.assertEquals(0,0, "Verified indeed 0 is 0");
    }


}
