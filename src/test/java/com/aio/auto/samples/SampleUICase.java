package com.aio.auto.samples;

import com.aiotests.automation.DriverManager;
import com.aiotests.automation.SoftAsserts;
import org.testng.annotations.Test;

public class SampleUICase {

    @Test(groups = "NVTES-TC-164")
    public void testGoogle() {
        DriverManager.get().get("https://www.google.com");
        SoftAsserts.getSoftAssert().assertEquals(2,2, "Verify that you are on google");
        SoftAsserts.getSoftAssert().assertEquals(true,false, "Verify that there is a search button");
    }
}
