package com.aio.auto.samples;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TestNGDataProviderTests {

    @DataProvider(name = "testDataProvider")
    public Object[][] testDataProvider() {
        return new Object[][] {
                {  37, 37, "NVTES-TC-161"},
                {  36, 35, "NVTES-TC-162" },
                {  35, 34, ""},
        };
    }

    @Test(dataProvider = "testDataProvider", groups = "SCRUM-TC-1621")
    public void dataProviderTestDefGroupAndOneOverriden(Integer n2, Integer n1, String tcName) {
        //Your test goes here
        System.out.println( n2);
        Assert.assertEquals(n1, n2, "Verify " + n1 + " == " + n2);
    }


}
